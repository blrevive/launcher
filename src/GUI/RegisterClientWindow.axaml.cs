using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using System;
using System.Linq;
using System.IO;
using Launcher.Utils;
using Launcher.Models;
using Launcher.Controller;


namespace Launcher.GUI
{
    public class RegisterClientWindow : Window
    {
        public RegisterClientWindow()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void OnSaveClick(object sender, RoutedEventArgs e)
        {
            var PathTextBox = this.Find<TextBox>("ClientFullPath");
            var AliasTextBox = this.Find<TextBox>("ClientAlias");

            try 
            {
                if(String.IsNullOrEmpty(PathTextBox.Text))
                    throw new UserInputException("Path must be specified!");

                if (!String.IsNullOrEmpty(AliasTextBox.Text))
                    GameClient.Add(PathTextBox.Text, AliasTextBox.Text);
                else 
                    GameClient.Add(PathTextBox.Text);
                this.Close();
            } 
            catch(UserInputException ex) 
            {
                ErrorHandler.Handle(ex);
            }
        }

        public async void OnBrowseClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog browser = new OpenFileDialog();
            var result = await browser.ShowAsync(this);
            if(result != null && result.Length == 1)
            {
                this.Find<TextBox>("ClientFullPath").Text = result[0];
            }
        }
    }
}