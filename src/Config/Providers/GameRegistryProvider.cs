using Launcher.Utils;
using System.Collections.Generic;
using Launcher.Models;

namespace Launcher.Configuration
{
    /// <summary>
    /// Container for GameRegistry
    /// </summary>
    public class GameRegistryProvider : IConfigProvider
    {
        public static string FileName = "GameRegistry.json";

        /// <summary>
        /// list of registered clients
        /// </summary>
        public List<GameClient> Clients { get; set; }

        public int DefaultClient { get; set; }
    }
}