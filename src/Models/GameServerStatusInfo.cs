﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Models
{
    public class GameServerStatusInfo
    {
        public GameServer Owner { get; set; }

        public int PID { get; set; }
        public int Port { get; set; }
        public float GameVersion { get; set; }
        public string Region { get; set; }
        public string ServerName { get; set; }

        public int BotCount { get; set; }
        public int MaxPlayers { get; set; }
        public int CurrentPlayers { get; set; }

        public string CurrentMap { get; set; }
        public string GameType { get; set; }

        public GameServerStatusInfo() { Owner = null; }
        public GameServerStatusInfo(GameServer owner) { Owner = owner; }

        public void SetOwner(GameServer owner) 
        {
            if (owner != null && Owner == null)
                Owner = owner;
        }
    }
}
