using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using Launcher.Utils;
using System.IO;
using Launcher.Configuration;
using System;
using System.Collections.Generic;
using Serilog;
using Launcher.Models;
using Launcher.Controller;
using System.Linq;
using System.Diagnostics;

namespace Launcher.GUI.Tabs
{
    public class ServerTab : UserControl
    {
        protected TextBox ServerNameInput;
        protected TextBox ServerDescription;
        protected TextBox ServerAddressInput;
        protected NumericUpDown ServerPortInput;
        protected ComboBox ClientInput;
        protected ComboBox PlaylistInput;
        protected ComboBox GamemodeInput;
        protected ComboBox MapInput;
        protected NumericUpDown MaxPlayersInput;
        protected NumericUpDown BotCountInput;
        protected DataGrid LaunchOptionsGrid;

        protected CheckBox RegisterToLobbyServersCheckbox;

        protected ComboBox RegionInput;

        protected NumericUpDown StatusServerPortInput;

        private DataGrid StatusServerGrid;
        private Button ShutdownGameServerButton;

        public ServerTab()
        {
            InitializeComponent();
            
            ServerNameInput = this.Find<TextBox>("ServerName");
            ServerDescription = this.Find<TextBox>("ServerDescription");
            ServerAddressInput = this.Find<TextBox>("ServerAddress");
            ServerPortInput = this.Find<NumericUpDown>("ServerPort");
            ClientInput = this.Find<ComboBox>("ClientSelect");
            PlaylistInput = this.Find<ComboBox>("PlaylistSelect");
            GamemodeInput = this.Find<ComboBox>("GamemodeSelect");
            MapInput = this.Find<ComboBox>("MapSelect");
            MaxPlayersInput = this.Find<NumericUpDown>("MaxPlayers");
            BotCountInput = this.Find<NumericUpDown>("BotCount");
            RegisterToLobbyServersCheckbox = this.Find<CheckBox>("RegisterToLobbyServer");
            RegionInput = this.Find<ComboBox>("ServerRegion");
            StatusServerPortInput = this.Find<NumericUpDown>("StatusServerPortInput");
            LaunchOptionsGrid = this.Find<DataGrid>("LaunchOptionsGrid");
            LaunchOptionsGrid.CellEditEnded += OnEditServerLaunchOptions;
            if (App.Data != null)
                App.Data.ServerLaunchOptionsGrid = LaunchOptionsGrid;

            StatusServerGrid = this.Find<DataGrid>("StatusServerGrid");
            ShutdownGameServerButton = this.Find<Button>("ShutdownGameServerButton");
            StatusServerGrid.SelectionChanged += 
                (x, y) => ShutdownGameServerButton.IsEnabled = StatusServerGrid.SelectedItem != null && (StatusServerGrid.Items as List<GameServerStatusInfo>).Count != 0;
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void OnEditServerLaunchOptions(object sender, DataGridCellEditEndedEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                App.Data.UpdateServerLaunchOptions((ObservableDictionary<string, string>)((DataGrid)sender).Items);
            }
        }

        public void OnLaunchClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var client = (GameClient)ClientInput.SelectedItem;

                if (client == null)
                    throw new UserInputException("Please select a client!");

                client.StartServer(new GameServerOptions(){
                    Port = (int)ServerPortInput.Value,
                    Name = ServerNameInput.Text,
                    Region = (string)RegionInput.SelectedItem,
                    Map = (string)MapInput.SelectedItem,
                    Playlist = (string)PlaylistInput.SelectedItem,
                    GameType = (string)GamemodeInput.SelectedItem,
                    BotCount = (int)BotCountInput.Value,
                    MaxPlayers = (int)MaxPlayersInput.Value,
                    Custom = Config.Game.ServerLaunchOptions
            }, (bool)RegisterToLobbyServersCheckbox.IsChecked);
                MessageBox.Avalonia.MessageBoxManager
                    .GetMessageBoxStandardWindow("Server", $"Server was started {((bool)RegisterToLobbyServersCheckbox.IsChecked ? "and registered" : "")}!")
                    .Show();
            } catch(UserInputException ex)
            {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", ex.Message).Show();
            }
        }

        public void StartStatusServer(object sender, RoutedEventArgs e)
        {
            Utils.StatusServer.Start(null, (ushort)StatusServerPortInput.Value);
        }

        public void StopStatusServer(object sender, RoutedEventArgs e)
        {
            Utils.StatusServer.Stop();
        }

        public void ShutdownGameServer(object sender, RoutedEventArgs e)
        {
            try
            {
                var info = StatusServerGrid.SelectedItem as GameServerStatusInfo;
                Process.GetProcessById(info.PID).Kill();
            } catch(Exception ex)
            {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", "Failed to stop server!").Show();
                Log.Fatal(ex, "Failed to stop server!");
            }
        }
    }
}