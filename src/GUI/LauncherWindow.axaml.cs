using System;
using Avalonia;
using Avalonia.Diagnostics;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Launcher.Configuration;
using Launcher.Utils;
using Launcher.GUI.Tabs;
using System.Collections.Generic;
using Serilog;

namespace Launcher.GUI
{
    public class LauncherWindow : Window
    {
        public static LauncherWindow Window;

        public LauncherWindow()
        {
            try
            {
                // initialize app
                #if DEBUG
                Logging.Initialize(false, true);
                this.AttachDevTools();
                #else
                Logging.Initialize();
                #endif
            } catch(Exception ex) {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", $"Error while initializing app: {ex.Message}").Show();
                Environment.Exit(0x10001);
            }

            InitializeComponent();
            Window = this;
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}