﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Launcher.Utils;
using System.Security.Cryptography;
using System.Globalization;
using Launcher.Controller;
using System.Collections.ObjectModel;
using Launcher.Configuration;
using Serilog;
using System.Diagnostics;

namespace Launcher.Models
{
    public class GameClient : IController
    {
        #region configurable properties
        public string Alias { get; set; }
        public string InstallPath { get; set; }
        public string OriginalGameFile { get; set; }
        public int Version { get; set; }
        #endregion

        #region derived properties
        public static string DefaultBinarySubpath => Path.Join("Binaries", "Win32");
        public static string DefaultGameFileName => "FoxGame-win32-Shipping.exe";
        public string ClientGameFile => Version >= 300 ? "BLR.exe" : "BLR-Patched.exe";
        public string ServerGameFile => "BLR-Server.exe";
        public string BinaryPath => Path.Join(InstallPath, DefaultBinarySubpath);
        public string OriginalGameFilePath => Path.Join(BinaryPath, OriginalGameFile);
        public string ClientGameFilePath => Path.Join(BinaryPath, ClientGameFile);
        public string ServerGameFilePath => Path.Join(BinaryPath, ServerGameFile);

        public bool IsPatched => File.Exists(ClientGameFilePath); 
        #endregion

        #region static data
        public static Dictionary<int, string> VersionHashes => new Dictionary<int, string>()
        {
            {302, "0f4a732484f566d928c580afdae6ef01c002198dd7158cb6de29b9a4960064c7" },
            {301, "de08147e419ed89d6db050b4c23fa772338132587f6b533b6233733f9bce46c3" },
            {300, "1742df917761f9dc01b079ae2aad78ef2ff17562af1dad6ad6ea7cf3622fe7f6" },
            {120, "d4f9cec736a83f7930f04438344d35ff9f0e57212755974bd51f48ff89d303c4" }
        };

        private static ObservableCollection<GameClient> _clients = new ObservableCollection<GameClient>(Config.Registry.Clients);
        #endregion

        #region management

        public static ObservableCollection<GameClient> GetObserver() => _clients;
        public static List<GameClient> GetAll(Func<GameClient, bool> filter = null) => filter == null ? _clients.ToList() : _clients.Where(filter).ToList();
        public static GameClient Get(Func<GameClient, bool> filter) => _clients.FirstOrDefault(filter);
        public static GameClient Get(string Alias) => Get(c => c.Alias == Alias);

        public static int DefaultClientIndex
        {
            get { return Config.Registry.DefaultClient; }
            set { Config.Registry.DefaultClient = value; Config.Save(); }
        }

        public static GameClient DefaultClient => _clients.ElementAtOrDefault(DefaultClientIndex);

        public static GameClient Add(string ClientPath, string Alias = null)
        {
            try
            {
                var client = new GameClient(ClientPath);
                client.Validate();

                Config.Registry.Clients.Add(client);
                Config.Save();

                _clients.Add(client);
                return client;
            } catch(Exception ex) { ErrorHandler.Handle(ex); return null; }
        }

        public static bool Remove(Func<GameClient, bool> filter)
        {
            try
            {
                var client = _clients.FirstOrDefault(filter);
                if (client == null)
                    return false;

                Config.Registry.Clients.Remove(client);
                Config.Save();

                _clients.Remove(client);
                return true;
            } catch(Exception ex) { ErrorHandler.Handle(ex); return false; }
        }
        #endregion

        #region constructors

        /// <summary>
        /// empty constructor for json de-/serialization
        /// </summary>
        public GameClient() 
        {
        }

        public GameClient(string ClientPath, string alias = null)
        {
            if (!File.Exists(ClientPath))
            {
                if (!Directory.Exists(ClientPath))
                    throw new ArgumentException("Path is neither valid file or directory", nameof(ClientPath));

                ClientPath = Path.Join(ClientPath, DefaultBinarySubpath, DefaultGameFileName);
                if (!File.Exists(ClientPath))
                    throw new ArgumentException("Path is neither valid file or directory", nameof(ClientPath));
            }

            OriginalGameFile = Path.GetFileName(ClientPath);
            InstallPath = Path.GetDirectoryName(ClientPath).Replace(DefaultBinarySubpath, "");
            Version = VersionHashes.Where(x => x.Value == Utils.Hash.StringFromFile(OriginalGameFilePath)).FirstOrDefault().Key;

            if (alias == null)
            {
                string ver = Decimal.Divide(Version, 100).ToString(CultureInfo.InvariantCulture);
                if (Get(ver) == null)
                    Alias = ver;
            } else
            {
                Alias = alias;
            }
        }
        #endregion

        #region validation
        public void Validate()
        {
            if (!Directory.Exists(InstallPath))
                throw new UserInputException($"Install path does not exist: {InstallPath}");

            if (Get(c => c.InstallPath == InstallPath) != null)
                throw new UserInputException($"Client already registered: {InstallPath}");

            if (!Directory.Exists(BinaryPath))
                throw new UserInputException($"Binary path does not exist: {BinaryPath}");

            if (!File.Exists(OriginalGameFilePath))
                throw new UserInputException($"Original Gamefile does not exist: {OriginalGameFilePath}");

            var hash = Utils.Hash.StringFromFile(OriginalGameFilePath);
            if (!VersionHashes.ContainsValue(hash))
                throw new UserInputException($"Version for hash {hash} is not supported!");
        }

        public bool IsValid()
        {
            try { Validate(); return true; }
            catch (Exception) { return false; }
        }
        #endregion

        public override string ToString() => Alias;

        /// <summary>
        /// Start client instance.
        /// </summary>
        /// <param name="options">client options</param>
        public static void StartClient(GameClient client, GameClientOptions options)
        {
            if (String.IsNullOrWhiteSpace(options.UserName))
                throw new UserInputException("Failed to start client: playername is invalid!");

            string resolvedIP = Network.GetHostIp(options.Server.Owner.Address);
            if (!Network.IsValidIPv4(resolvedIP))
                throw new UserInputException("Failed to start client: ip adress is invalid!");

            string parms = $"?Name={options.UserName}";
            string URL;
            if (options.CustomAddress != null)
                URL = $"{options.CustomAddress}{parms}";
            else
                URL = $"{resolvedIP}:{options.Server.Port}{parms}";

            if (options.Custom != null)
                foreach (var opt in options.Custom)
                    URL += opt.Value != null ? $"?{opt.Key}={opt.Value}" : $"?{opt.Key}";

            StartGame(client.ClientGameFile, URL, client.BinaryPath);
        }

        public void StartClient(GameClientOptions options) => StartClient(this, options);

        /// <summary>
        /// Start server instance.
        /// </summary>
        /// <param name="options">server options</param>
        public static void StartServer(GameClient client, GameServerOptions options, bool register = false)
        {
            if (client == null)
                throw new UserInputException("Invalid client (was null)");

            if (options.Name == null)
                throw new UserInputException("ServerName must be specified!");
            if (options.Name.Contains('\\') || options.Name.Contains('/'))
                throw new UserInputException("No slashes are allowed in server title!");

            if (options.Playlist == "None")
                options.Playlist = "";

            string url = $"server {options.Map}?Name={options.Name.Replace(' ', '-')}?Game=FoxGame.FoxGameMP_{options.GameType}?Port={options.Port}" +
                $"?NumBots={options.BotCount}?MaxPlayers={options.MaxPlayers}?Playlist={options.Playlist}?Region={options.Region}";

            if (options.Custom != null)
                foreach (var opt in options.Custom)
                    url += opt.Value != null ? $"?{opt.Key}={opt.Value}" : $"?{opt.Key}";

            StartGame(register ? client.ServerGameFile : client.OriginalGameFile, url, client.BinaryPath);

            if (register && !Utils.StatusServer.IsRunning)
                Utils.StatusServer.Start();
        }

        public void StartServer(GameServerOptions opts, bool register = false) => StartServer(this, opts, register);
    }
}
