using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Controls.Primitives;

namespace Launcher.GUI.Controls
{
    /// <summary>
    /// Represents a section inside the tabs.
    /// </summary>
    public class TabSection : ContentControl
    {
        public static readonly StyledProperty<string> HeaderProperty = AvaloniaProperty.Register<TabSection, string>(nameof(Header));
        public static readonly DirectProperty<TabSection, bool> IsExpandedProperty = AvaloniaProperty.RegisterDirect<TabSection, bool>(
            nameof(IsExpanded),
            o => o.IsExpanded,
            (o, v) => o.IsExpanded = v,
            defaultBindingMode: Avalonia.Data.BindingMode.TwoWay);

        private bool _isExpanded = true;

        public static readonly DirectProperty<TabSection, bool> IsNotDefaultProperty = AvaloniaProperty.RegisterDirect<TabSection, bool>(
            nameof(IsNotDefault),
            o => !o._isDefault,
            (o, v) => o._isDefault = !v
        );

        private bool _isDefault = false;
        public static readonly DirectProperty<TabSection, bool> IsDefaultProperty = AvaloniaProperty.RegisterDirect<TabSection, bool>(
            nameof(IsDefault),
            o => o._isDefault,
            (o, v) => o._isDefault = v);

        public TabSection()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public string Header
        {
            get { return GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set 
            { 
                SetAndRaise(IsExpandedProperty, ref _isExpanded, value);
                PseudoClasses.Set(":expanded", value);
            }
        }

        public bool IsDefault 
        {
            get { return GetValue(IsDefaultProperty); }
            set { SetValue(IsDefaultProperty, value); }
        }

        public bool IsNotDefault
        {
            get { return !IsDefault; }
            set { IsDefault = !value; }
        }
    }
}