﻿using Launcher.Controller;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Launcher.Configuration;
using Launcher.Utils;
using System.Collections.ObjectModel;
using System.Text.Json.Serialization;
using Launcher.GUI;

namespace Launcher.Models
{
    /// <summary>
    /// Representation of a game server
    /// </summary>
    public class GameServer : IController
    {
        #region properties
        [JsonPropertyName("address")]
        public string Address { get; set; }
        public IPAddress IP { get; private set; }
        public bool HasIP => IP != null;
        public string Domain { get; private set; }
        public bool HasDomain => Domain != null;

        [JsonPropertyName("port")]
        public ushort Port { get; set; }

        [JsonPropertyName("uri")]
        public string StatusPath { get; set; }

        public string StatusURL => $"{Protocol}://{Address}:{Port}/{StatusPath}";
        public override string ToString() => StatusURL;

        public string Protocol { get; set; }

        public bool IsUnregistered { get; set; }
        #endregion

        public static List<GameServer> Servers => new List<GameServer>();

        public static ObservableCollection<GameServerStatusInfo> Favorites = new ObservableCollection<GameServerStatusInfo>(Config.Hosts.Hosts);

        public static void AddFavorite(string Name, string Address, ushort Port)
        {
            var gServer = new GameServer(Address, Port, "", true);
            var info = new GameServerStatusInfo(gServer);
            info.Port = Port;
            info.ServerName = Name;
            AddFavorite(info);
        }

        public static void AddFavorite(GameServerStatusInfo info)
        {
            Favorites.Add(info);
            Config.Hosts.Hosts = Favorites.ToList();
            Config.Save();
        }

        public static void RemoveFavorite(GameServerStatusInfo server) 
        {
            Favorites.Remove(server);
            Config.Hosts.Hosts = Favorites.ToList();
            Config.Save();
        }

        public GameServer() 
        {
            Protocol = "http";
            Servers.Add(this);
        }

        ~GameServer()
        {
            if(Servers.Contains(this)) 
                Servers.Remove(this);
        }

        public GameServer(Uri uri, bool unregistered = false) : this()
        {
            switch(uri.HostNameType)
            {
                case UriHostNameType.IPv4:
                case UriHostNameType.IPv6:
                    IP = IPAddress.Parse(uri.Host);
                    Domain = null;
                    break;

                case UriHostNameType.Dns:
                    IP = null;
                    Domain = uri.Host;
                    break;
            }

            Protocol = uri.Scheme;
            Port = (ushort)uri.Port;
            StatusPath = uri.LocalPath;
            IsUnregistered = unregistered;
        }

        public GameServer(string URL, bool unregistered = false) : this(new Uri(URL), unregistered) { }

        public GameServer(string protocol, string domain, ushort port, string statusPath, bool unregistered = false) : this()
        {
            Protocol = protocol;
            Domain = Address = domain;
            Port = port;
            StatusPath = statusPath;
            IsUnregistered = unregistered;
        }

        public GameServer(string domain, ushort port, string statusPath, bool unregistered = false) : this("http", domain, port, statusPath, unregistered) { }

        public async Task<List<GameServerStatusInfo>> PullStatusInfo()
        {
            /*if(IsUnregistered)
            {
                var info = new GameServerStatusInfo(this);
                info.Port = this.Port;
                return new List<GameServerStatusInfo>() { info };
            }*/

            try
            {
                using (var http = new HttpClient())
                {
                    var raw = await http.GetStringAsync(StatusURL);
                    var res = JsonSerializer.Deserialize<List<GameServerStatusInfo>>(raw, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                    res.ForEach(si => si.SetOwner(this));
                    return res;
                }
            } catch (Exception e)
            {
                Log.Debug($"Failed to pull status info from {this}");
                return new List<GameServerStatusInfo>();
            }
        }
    }
}
