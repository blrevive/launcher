﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Models
{
    /// <summary>
    /// Launch options for client
    /// </summary>
    public class GameClientOptions
    {
        /// <summary>
        /// Gameserver to connect to
        /// </summary>
        public GameServerStatusInfo Server { get; set; }

        /// <summary>
        /// Address override if no game server instance is given
        /// </summary>
        public string CustomAddress { get; set; }

        /// <summary>
        /// Password of server
        /// </summary>
        public string ServerPassword { get; set; }

        /// <summary>
        /// Name of the user
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Custom options
        /// </summary>
        public Dictionary<string, string> Custom;
    }
}
