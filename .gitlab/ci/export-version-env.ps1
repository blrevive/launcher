$env:VER_FULL = [regex]::Match($CI_COMMIT_TAG, '^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$').captures.groups[0].value
$env:VER_MAJOR = [regex]::Match($CI_COMMIT_TAG, '^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$').captures.groups[1].value
$env:VER_MINOR = [regex]::Match($CI_COMMIT_TAG, '^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$').captures.groups[2].value
$env:VER_PATCH = [regex]::Match($CI_COMMIT_TAG, '^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$').captures.groups[3].value
$env:VER_SUFFIX = "-" + [regex]::Match($CI_COMMIT_TAG, '^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$').captures.groups[4].value
$env:VER_RELEASE = -join($env:VER_MAJOR, ".", $env:VER_MINOR, ".", $env:VER_PATCH)