﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Launcher.Configuration;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Diagnostics;
using Launcher.Controller;
using Serilog;
using System.Threading;
using Launcher.Models;
using Launcher.GUI;
using EmbedIO;
using EmbedIO.Actions;
using System.Net.Sockets;

namespace Launcher.Utils
{
    /// <summary>
    /// A simple HTTP server which serves a status list of all available game servers on this machine.
    /// Exposes the list to /serverstatus
    /// </summary>
    public class StatusServer
    {
        protected static WebServer host = null;
        protected static Task infoUpdateTask = null;
        protected static CancellationTokenSource _mainThreadCTS;
        protected static CancellationTokenSource _infoThreadCTS;

        protected static Task mainThread;
        protected static Task infoThread;

        private static IPAddress ip;
        private static ushort port;

        public static Dictionary<int, GameServerStatusInfo> Data;

        public static bool IsRunning => host != null;

        private static List<string> ClearOutdatedStatusFiles(List<string> files)
        {
            var pids = files.Select(x => Convert.ToInt32(Path.GetFileName(x).Replace(".status.json", "")));

            var outdated = pids.Where(x => { try { 
                    return Process.GetProcessById(x).ProcessName != "BLR-Server"; } catch { return true; } }).ToList();
            outdated.ForEach(x => File.Delete(files.Where(f => f.Contains(x.ToString())).FirstOrDefault()));
            return files.Where(f => !outdated.Contains(Convert.ToInt32(Path.GetFileName(f).Replace(".status.json", "")))).ToList();
        }

        private static Dictionary<int, GameServerStatusInfo> GetServerStatusByPID()
        {
            var statusFiles = new Dictionary<int, GameServerStatusInfo>();
            foreach (var client in Config.Registry.Clients)
            {
                foreach (var file in ClearOutdatedStatusFiles(Directory.EnumerateFiles(client.BinaryPath, "*.status.json").ToList()))
                {
                    
                    int pid = Convert.ToInt32(Path.GetFileName(file).Replace(".status.json", ""));
                    string content = File.ReadAllText(file);
                    statusFiles.Add(pid, JsonSerializer.Deserialize < GameServerStatusInfo >( content));
                }
            }

            return statusFiles;
        }

        public static List<GameServerStatusInfo> GetServerStatusInfos() => GetServerStatusByPID().Values.ToList();

        /// <summary>
        /// Merges all status.json files from all known clients to one single json.
        /// </summary>
        /// <returns>merged status</returns>
        public static string GetAllServerStatus() 
        {
            if (Data == null)
                Data = GetServerStatusByPID();

            return $"[{String.Join(",", Data.Values)}]";

        }

        protected static WebServer CreateHost(IPAddress ip, ushort port)
        {
            var server = new WebServer(o => o.WithUrlPrefix($"http://+:{port}").WithMode(HttpListenerMode.EmbedIO))
                .WithModule(new ActionModule("/servers", HttpVerbs.Get, ctx => { Log.Information(ctx.Request.ContentType); return ctx.SendDataAsync(GetServerStatusInfos()); })) ;
            server.StateChanged += (s, e) => Log.Information($"WebServer New State - {e.NewState}");
            return server;
        }

        public static async void Start(IPAddress ip = null, ushort port = 8090)
        {
            try
            {
                if (ip == null)
                    ip = StatusServer.ip = IPAddress.Any;

                Log.Information($"Starting StatusServer on {ip}:{port}");
                if (host != null)
                {
                    ErrorHandler.Handle("Server is already running!");
                    return;
                }
                StatusServer.port = port;

                host = CreateHost(ip, port);
                _mainThreadCTS = new CancellationTokenSource();
                _infoThreadCTS = new CancellationTokenSource();
                mainThread = host.RunAsync(_mainThreadCTS.Token);

                if(! await AwaitPortBinding())
                {
                    Stop();
                    ErrorHandler.Handle("Port binding failed, please accept firewall request or add a rule manualy!");
                    return;
                }

                if (!await Register())
                {
                    Stop();
                    ErrorHandler.Handle("Failed to register server! (Did you add a firefwall rule for the launcher?)");
                    return;
                }

                infoThread = Task.Run(UpdateServers, _infoThreadCTS.Token);
                App.Data.IsStatusServerRunning = true;
            } catch(Exception e)
            {
                ErrorHandler.Handle(e);
            }
        }

        protected static async Task<bool> AwaitPortBinding(int timeOut = 10000)
        {
            var curr = 0;
            var remoteIp = await Utils.Network.GetRemoteIP();
            while (curr < timeOut)
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(1);
                    try { return (await client.GetAsync($"http://{remoteIp}:{port}/servers")).IsSuccessStatusCode; }
                    catch (Exception ex) when (ex is TaskCanceledException || ex is TimeoutException) { curr += 1000; continue; }
                    catch(Exception ex) { Log.Debug(ex, "E"); return false; }
                }
            }

            return false;
        }

        public static void Stop()
        {
            if (host == null)
            {
                ErrorHandler.Handle("Server is not runnning");
                return;
            }

            if(mainThread != null)
            {
                _mainThreadCTS.Cancel();
                mainThread.Wait();
                _mainThreadCTS = null;
                mainThread = null;
            }
            if(infoThread != null)
            {
                _infoThreadCTS.Cancel();
                infoThread.Wait();
                _infoThreadCTS = null;
                infoThread = null;
            }

            host = null;
            if(App.Data != null)
                App.Data.IsStatusServerRunning = false;
        }

        public static void UpdateServers()
        {
            int lastCount = 0;
            while(!_infoThreadCTS.IsCancellationRequested)
            {
                Data = GetServerStatusByPID();
                foreach(var kv in Data)
                    kv.Value.PID = kv.Key;

                if(lastCount != Data.Count)
                {
                    GUI.App.Data.LocalGameServers = Data.Values.ToList();
                    lastCount = Data.Count;
                }
                Thread.Sleep(5000);
            }
        }

        public static async Task<bool> Register(List<LobbyServer> lobbyServers = null)
        {
            if (lobbyServers == null)
                lobbyServers = await LobbyServer.GetAll();

            var ip = await Utils.Network.GetRemoteIP();
            var reqDataDict = new Dictionary<string, dynamic>()
            {
                {"address", ip },
                {"port", port },
                {"uri", "servers" }
            };

            var reqContent = new StringContent(JsonSerializer.Serialize(reqDataDict), Encoding.UTF8, "application/json");

            using (var http = new HttpClient())
            {
                foreach (var lobbyServer in lobbyServers)
                {
                    try
                    {
                        var result = await http.PostAsync(lobbyServer.URI, reqContent);
                        if(result.StatusCode != HttpStatusCode.OK)
                        {
                            if (result.Content.ReadAsStringAsync().Result.Contains("host already registered"))
                                return true;
                            Log.Debug($"Failed to register to server {lobbyServer}: {result.Content.ReadAsStringAsync().Result}");
                            return false;
                        }
                    } catch(Exception ex)
                    {
                        Log.Debug(ex, "Failed to retrieve data");
                        continue;
                    }
                }
                return true;
            }
        }
    }
}
