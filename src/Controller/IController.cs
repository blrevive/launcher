﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Launcher.Utils;
using Serilog;

namespace Launcher.Controller
{
    public abstract class IController
    {
        /// <summary>
        /// Start a game instance with given parameters.
        /// </summary>
        /// <param name="filename">BLR app</param>
        /// <param name="args">parameters</param>
        /// <param name="workdir">path to BLR</param>
        protected static void StartGame(string filename, string args, string workdir)
        {
            if (!Directory.Exists(workdir))
                throw new UserInputException("Starting game failed: directory does not exist!");

            if (!File.Exists(Path.Join(workdir, filename)))
                throw new UserInputException($"Starting game failed: file does not exist: {filename}!");

            ProcessStartInfo si = new ProcessStartInfo()
            {
                WorkingDirectory = workdir,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            #if LINUX
            si.FileName = "wine";
            si.Arguments = $"\"{filename}\" {args}";
            #else
            si.FileName = Path.Join(workdir, filename);
            si.Arguments = args;
            #endif

            try
            {
                Log.Debug($"Starting game ({filename} {args})");
                Process gameProcess = new Process();
                gameProcess.StartInfo = si;
                gameProcess.Start();
            }
            catch (Exception ex)
            {
                throw new UserInputException($"Failed to launch game process: {ex.Message}", ex);
            }
        }
    }
}
