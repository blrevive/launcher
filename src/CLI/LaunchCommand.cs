using System;
using System.IO;
using CommandLine;
using Launcher.Utils;
using Launcher.Configuration;
using Launcher.Models;
using Launcher.Controller;


namespace Launcher.CLI.Commands
{

    public enum LaunchType
    {
        Client,
        Server
    }

    /// <summary>
    /// Command to launch a client instance.
    /// </summary>
    [Verb("launch", HelpText = "Launch game client or server")]
    public class LaunchCommand
    {
        [Value(0, MetaName = "Type", HelpText = "Which type of game should be launcher (client/server)")]
        public LaunchType Type { get; set; }

        [Option('u', "url", HelpText = "Unreal URL passed to game command line", Default = "localhost?Name=Player")]
        public string URL { get; set; }

        [Option('i', "ip", HelpText = "IP adress of server when launching client", Default = "127.0.0.1")]
        public string IP { get; set; }

        [Option('p', "port", HelpText = "Port of server to connect when launching client", Default = 7777)]
        public int Port { get; set; }

        [Option('a', "alias", HelpText = "Alias for game client to launch")]
        public string Alias { get; set; }

        [Option('r', "register", HelpText = "Register to lobby server (server only)", Default = true)]
        public bool Register { get; set; }

        [Option('n', "name", HelpText = "PlayerName", Default = "Agent Anon")]
        public string PlayerName { get; set; }

        public void Execute()
        {
            throw new NotImplementedException();
        }
    }
}