using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Launcher.Configuration;
using Launcher.Utils;
using Launcher.Models;
using Launcher.Controller;
using System.Diagnostics;
using System.Threading.Tasks;
using Avalonia.Controls;
using System.Linq;

namespace Launcher.GUI.ViewModels
{

    public class LauncherWindowModel : ViewModelBase
    {
        public LauncherWindowModel()
        {
            ClientLaunchOptions = new ObservableDictionary<string, string>(Config.Game.ClientLaunchOptions);
            ServerLaunchOptions = new ObservableDictionary<string, string>(Config.Game.ServerLaunchOptions);
            GameServers = new ObservableCollection<GameServerStatusInfo>();
            RemoteIP = String.Empty;
            LoadAsyncData();
        }

        public void LoadAsyncData()
        {
            Task.Run(async () => GameServers = new ObservableCollection<GameServerStatusInfo>(await LobbyServer.GetAllServerInfos()));
            Task.Run(async () => RemoteIP = await Network.GetRemoteIP());
        }

        private int _defaultClientIndex = GameClient.DefaultClientIndex;
        public int DefaultClientIndex
        {
            get { return _defaultClientIndex; }
            set { GameClient.DefaultClientIndex = value; SetProperty(ref _defaultClientIndex, value); }
        }

        public ObservableCollection<GameClient> Clients => GameClient.GetObserver();

        public string[] Maps { get { return Config.Game.Maps; } }
        public string[] GameModes { get { return Config.Game.Gamemodes; } }
        public string[] Playlists { get { return Config.Game.Playlists; } }

        private string _userName = Config.User.Username;
        public string UserName
        {
            get { return _userName; }
            set { Config.User.Username = value; Config.Save(); SetProperty(ref _userName, value); }
        }

        private ObservableCollection<GameServerStatusInfo> _gameServers = new ObservableCollection<GameServerStatusInfo>();
        public ObservableCollection<GameServerStatusInfo> GameServers
        {
            get { return _gameServers; }
            set { this.SetProperty(ref _gameServers, value); }
        }

        private ObservableCollection<GameServerStatusInfo> _favoriteServers = GameServer.Favorites;
        public ObservableCollection<GameServerStatusInfo> FavoriteServers
        {
            get { return _favoriteServers; }
            set { SetProperty(ref _favoriteServers, value); }
        }

        private List<GameServerStatusInfo> _localGameServers = null;
        public List<GameServerStatusInfo> LocalGameServers
        {
            get { return _localGameServers; }
            set { SetProperty(ref _localGameServers, value); }
        }

        private bool _isStatusServerRunning = StatusServer.IsRunning;
        public bool IsStatusServerRunning
        {
            get { return _isStatusServerRunning; }
            set { SetProperty(ref _isStatusServerRunning, value); }
        }

        public List<string> Regions
        {
            get { return Config.Server.Regions; }
        }

        private string _defaultRegion = Config.Server.DefaultRegion;
        public string DefaultRegion
        {
            get { return _defaultRegion; }
            set { Config.Server.DefaultRegion = value; Config.Save(); this.SetProperty(ref _defaultRegion, value); }
        }

        private string _remoteIp = String.Empty;
        public string RemoteIP
        {
            get { return _remoteIp; }
            private set { SetProperty(ref _remoteIp, value); }
        }

        public ObservableDictionary<string, string> ClientLaunchOptions { get; set; }
        public DataGrid ClientLaunchOptionsGrid = null;

        public ObservableDictionary<string, string> ServerLaunchOptions { get; set; }
        public DataGrid ServerLaunchOptionsGrid = null;

        public void AddNewClientLaunchOption() 
        { 
            try
            {
                ClientLaunchOptions.Add("", "");
            } 
            catch(Exception e) 
            {
                Debug.WriteLine(e.Message);
            }
        }

        public void AddNewServerLaunchOption()
        {
            try
            {
                ServerLaunchOptions.Add("", "");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        public void RemoveClientLaunchOption()
        {
            try
            {
                var opt = (ObservableKeyValuePair<string, string>)ClientLaunchOptionsGrid.SelectedItem;
                if (ClientLaunchOptions.Contains(opt))
                    ClientLaunchOptions.Remove(opt);
                Config.Game.ClientLaunchOptions = ClientLaunchOptions.ToDictionary();
                Config.Save();

            } catch(Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        public void RemoveServerLaunchOption()
        {
            try
            {
                var opt = (ObservableKeyValuePair<string, string>)ServerLaunchOptionsGrid.SelectedItem;
                if (ServerLaunchOptions.Contains(opt))
                    ServerLaunchOptions.Remove(opt);
                Config.Game.ServerLaunchOptions = ServerLaunchOptions.ToDictionary();
                Config.Save();

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        public void UpdateClientLaunchOptions(ObservableDictionary<string, string> newValue)
        {
            ClientLaunchOptions = newValue;
            Config.Game.ClientLaunchOptions = newValue.ToDictionary();
            Config.Save();
        }

        public void UpdateServerLaunchOptions(ObservableDictionary<string, string> newValue)
        {
            ServerLaunchOptions = newValue;
            Config.Game.ServerLaunchOptions = newValue.ToDictionary();
            Config.Save();
        }
    }
}