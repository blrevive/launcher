#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

PROJECT_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}"
TAGS_URL="${PROJECT_URL}/tags"

function get_latest_version {
	curl --header "PRIVATE-TOKEN: ${CI_JOB_TOKEN}" "$TAGS_URL"
}

if [[ -v CI_COMMIT_TAG ]]
then
	export VER_MAJOR=$(${DIR}/semver.sh get major ${CI_COMMIT_TAG})
	export VER_MINOR=$(${DIR}/semver.sh get minor ${CI_COMMIT_TAG})
	export VER_PATCH=$(${DIR}/semver.sh get patch ${CI_COMMIT_TAG})
	export VER_SUFFIX="-$(${DIR}/semver.sh get prerel ${CI_COMMIT_TAG})"
	export VER_RELEASE=$(${DIR}/semver.sh get release ${CI_COMMIT_TAG})
else
	echo "No tag given"
fi