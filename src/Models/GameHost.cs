﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Launcher.Models
{
    /// <summary>
    /// Model representing a game server host.
    /// </summary>
    public class GameHost
    {
        /// <summary>
        /// Static localhost 
        /// </summary>
        public static GameHost LocalHost => new GameHost("http://127.0.0.1:8080/servers");

        /// <summary>
        /// absolute URL to status server
        /// </summary>
        public string StatusURL => $"{Protocol}://{Address}:{Port}/{Suffix}";

        /// <summary>
        /// protocol (http/https)
        /// </summary>
        public string Protocol = "http";

        /// <summary>
        /// hostname or IP
        /// </summary>
        [JsonPropertyName("address")]
        public string Address { get; set; }

        /// <summary>
        /// port
        /// </summary>
        [JsonPropertyName("port")]
        public int Port { get; set; }

        /// <summary>
        /// suffix (sub-url to status server)
        /// </summary>
        [JsonPropertyName("uri")]
        public string Suffix { get; set; }

        public GameHost() { }

        /// <summary>
        /// Create new game host.
        /// </summary>
        /// <param name="URL">URL to status server</param>
        public GameHost(Uri URL)
        {
            this.Protocol = URL.Scheme;
            this.Address = URL.Host;
            this.Port = URL.Port;
        }

        /// <summary>
        /// Create new game host.
        /// </summary>
        /// <param name="Port">Port</param>
        /// <param name="Address">Hostname/IP</param>
        /// <param name="Suffix">Suffix to status server</param>
        /// <param name="Protocol">Protocol</param>
        public GameHost(string Address, int Port, string Suffix, string Protocol = "http")
        {
            this.Port = Port;
            this.Address = Address;
            this.Suffix = Suffix;
            this.Protocol = Protocol;
        }

        public GameHost(string Address) : this(Address, 0, String.Empty) { }
    }
}
