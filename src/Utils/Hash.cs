﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Utils
{
    public static class Hash
    {
        /// <summary>
        /// Create a SHA256 hash of a file and optionaly save it to disk.
        /// </summary>
        /// <param name="path">File to hash</param>
        /// <param name="saveHash">save hash to file</param>
        /// <returns>SHA256 file hash</returns>
        public static byte[] FromFile(string path, bool saveHash = true)
        {
            try
            {
                var dir = Path.GetDirectoryName(path);
                var hashFile = Path.Join(dir, Path.GetFileName(path) + ".sha256");

                if (saveHash)
                    if (File.Exists(hashFile))
                        return File.ReadAllBytes(hashFile);

                var hash = SHA256.Create().ComputeHash(File.ReadAllBytes(path));

                if (saveHash)
                    File.WriteAllBytes(hashFile, hash);

                return hash;
            }
            catch (Exception e)
            {
                ErrorHandler.Handle(e);
                return null;
            }
        }

        /// <summary>
        /// Get hash as string of a file.
        /// </summary>
        /// <param name="path">file to hash</param>
        /// <param name="saveHash">save hash to file</param>
        /// <returns>hash of file</returns>
        public static string StringFromFile(string path, bool saveHash = true) => String.Join(null, FromFile(path, saveHash).ToList().Select(x => x.ToString("x2")));
    }
}
