﻿using System;
using System.Collections.Generic;
using Launcher.Models;

namespace Launcher.Configuration
{
    /// <summary>
    /// Container for known hosts
    /// </summary>
    public class ServerHostsConfigProvider : IConfigProvider
    {

        /// <summary>
        /// Contains the server connected to in the previous session.
        /// </summary>
        public GameServer PreviousHost { get; set; }

        /// <summary>
        /// Known Host Servers by IP or Name and port  
        /// </summary>
        public List<GameServerStatusInfo> Hosts { get; set; }

        public int MaxClientHostListSize = 50;
    }
}
