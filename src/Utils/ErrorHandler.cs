﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using Launcher.GUI;

namespace Launcher.Utils
{
    public static class ErrorHandler
    {

        public static void Handle(Exception ex)
        {
            Log.Debug(ex, "handled exception");
            Handle(ex.Message);
        }

        public static void Handle(string Message)
        {
            if(App.Data != null)
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", Message).Show();
        }
    }
}
