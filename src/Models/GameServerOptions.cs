﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Models
{
    /// <summary>
    /// Launch options for server
    /// </summary>
    public class GameServerOptions
    {
        /// <summary>
        /// Port to listen on
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Region of server
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Name of server
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Password of server
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// List of user names to give admin priviligues
        /// </summary>
        public string[] Admins { get; set; }

        /// <summary>
        /// Number of bots
        /// </summary>
        public int BotCount { get; set; }

        /// <summary>
        /// Maximum allowed players
        /// </summary>
        public int MaxPlayers { get; set; }

        /// <summary>
        /// Maximum allowed spectators
        /// </summary>
        public int MaxSpectators { get; set; }

        /// <summary>
        /// Playlist of server
        /// </summary>
        public string Playlist { get; set; }

        /// <summary>
        /// Initial GameType 
        /// </summary>
        public string GameType { get; set; }

        /// <summary>
        /// Initial Map
        /// </summary>
        public string Map { get; set; }

        /// <summary>
        /// Goal score
        /// </summary>
        public int GoalScore { get; set; }

        /// <summary>
        /// Timelimit (in minutes)
        /// </summary>
        public int TimeLimit { get; set; }

        /// <summary>
        /// Custom options
        /// </summary>
        public Dictionary<string, string> Custom;
    }
}
