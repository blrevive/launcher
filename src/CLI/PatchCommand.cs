using System;
using System.IO;
using CommandLine;
using Launcher.Controller;
using Launcher.Utils;
using Serilog;

namespace Launcher.CLI.Commands
{
    /// <summary>
    /// Command for patching clients.
    /// </summary>
    [Verb("patch", HelpText = "Patch a BL:R game file")]
    public class PatchCommand
    {
        [Value(0, MetaName = "Input", HelpText = "Client alias.", Required = true)]
        public string Alias { get; set; }

        [Value(1, MetaName = "Output", HelpText = "Patched file output. If empty, using <Input>-Patched.exe", Required = false)]
        public string Output { get; set; }

        [Option('i', "inject-proxy", HelpText = "Inject proxy", Default = false)]
        public bool InjectProxy { get; set; }

        [Option('p', "patch", HelpText = "Apply game patches", Default = true)]
        public bool ApplyPatches { get; set; }

        [Option('g', "gamefolder", HelpText = "Path to game binaries when using filenames as input")]
        public string Gamefolder { get; set; }

        public void Execute()
        {
            //Input = App.ParseClientIdentifier(Input, Gamefolder);
            var client = Models.GameClient.Get(Alias);

            if (client == null)
            {
                Log.Error($"No client with alias {Alias} found!");
                return;
            }

            PatchController.PatchGameFile(client, 
                () => Log.Information($"Client {Alias} was patched succesfully!"), 
                () => Log.Error($"Patching client {Alias} failed!"));

            //PatchController. PatchGameFile(Input, Output, ApplyPatches, InjectProxy);
        }
    }
}