﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using Launcher.Utils;
using Serilog;

namespace Launcher.CLI.Commands
{
    [Verb("statusserver", HelpText = "Start the status server")]
    public class StatusServerCommand
    {
        public void Execute()
        {
            Log.Information("Starting server");
            StatusServer.Start();
            Console.ReadLine();
            StatusServer.Stop();
        }
    }
}
