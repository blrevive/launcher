using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Launcher.Utils;
using Launcher.Configuration;
using Launcher.GUI.ViewModels;

namespace Launcher.GUI
{
    public class App : Application
    {
        public static LauncherWindowModel Data { get; protected set; }

        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
        
        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                Config.Load();

                if (Data == null)
                    Data = new LauncherWindowModel();


                desktop.MainWindow = new LauncherWindow()
                {
                    DataContext = Data
                };
            }

            base.OnFrameworkInitializationCompleted();
        }
   }
}