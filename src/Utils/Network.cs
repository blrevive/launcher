﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Serilog;
using Launcher.Configuration;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Launcher.Utils
{
    /// <summary>
    /// Provides common network util functions
    /// </summary>
    public class Network
    {
        private static string RemoteIP;

        /// <summary>
        /// Resolves the host IP based on host name or address
        /// </summary>
        /// <param name="hostNameOrAddress"></param>
        /// <returns>hostNameOrAddress if already a valid IPv4 or the IP based on host name</returns>
        public static string GetHostIp(string hostNameOrAddress)
        {
            if (IsValidIPv4(hostNameOrAddress))
            {
                return hostNameOrAddress;
            }

            string hostIp = null;
            try
            {
                IPAddress ipAddress = Dns.GetHostAddresses(hostNameOrAddress).Where(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).First();
                hostIp = ipAddress.ToString();
            }
            catch
            {
                Log.Error("Could not resolve host name: {0}", hostNameOrAddress);
            }

            return hostIp;
        }

        /// <summary>
        /// Validates an IPv4 Address Template
        /// </summary>
        /// <param name="ipString"></param>
        /// <returns></returns>
        public static bool IsValidIPv4(string ipString) => !String.IsNullOrWhiteSpace(ipString) && Regex.IsMatch(ipString, "^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\\.(?!$)|$)){4}$");

        /// <summary>
        /// Check if the specified address is a local adress (either local IP or DNS).
        /// </summary>
        /// <param name="host">address to check</param>
        /// <returns></returns>
        public static bool IsLocalIpAddress(string host)
        {
            try
            {
                // get host IP addresses
                IPAddress[] hostIPs = Dns.GetHostAddresses(host);
                // get local IP addresses
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

                // test if any host IP equals to any local IP or to localhost
                foreach (IPAddress hostIP in hostIPs)
                {
                    // is localhost
                    if (IPAddress.IsLoopback(hostIP)) return true;
                    // is local address
                    foreach (IPAddress localIP in localIPs)
                    {
                        if (hostIP.Equals(localIP)) return true;
                    }
                }
            }
            catch { }
            return false;
        }

        /// <summary>
        /// Get the remote ip address of this machine.
        /// </summary>
        /// <returns>remote IP</returns>
        public static async Task<string> GetRemoteIP() => String.IsNullOrEmpty(RemoteIP) ? await (new HttpClient().GetStringAsync("https://ip4.seeip.org/")) : RemoteIP;

        /// <summary>
        /// Check if the (remote) address belongs to this machine.
        /// </summary>
        /// <param name="host">address to check</param>
        /// <returns>whether address points to this machine</returns>
        public static async Task<bool> IsOwnAddress(string host) => GetHostIp(host) == await GetRemoteIP() || IsLocalIpAddress(host);

        public static bool IsAlive(string URI)
        {
            HttpWebRequest request = WebRequest.CreateHttp(URI);
            request.Timeout = 3000;
            request.Method = "HEAD";

            try
            {
                using(HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
                {
                    return resp.StatusCode == HttpStatusCode.OK;
                }
            } catch(WebException e)
            {
                return false;
            }
        }
    }
}