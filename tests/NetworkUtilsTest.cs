using NUnit.Framework;
using Launcher.Utils;
using System.Threading.Tasks;

namespace Tests
{
    public class NetworkUtilsTest
    {
        [Test]
        public async Task TestOwnAddress()
        {
            var remoteIp = await Network.GetRemoteIP();
            Assert.IsTrue(await Network.IsOwnAddress(remoteIp), "Is remote IP");
            Assert.IsTrue(await Network.IsOwnAddress("127.0.0.1"));
            Assert.IsTrue(await Network.IsOwnAddress("localhost"));
            Assert.IsFalse(await Network.IsOwnAddress("8.8.8.8"));
        }

        [Test]
        public void TestAlive()
        {
            Assert.IsTrue(Network.IsAlive("https://www.google.de"));
            Assert.IsFalse(Network.IsAlive("https://some.non.existant.url"));
        }

        [Test]
        public void TestLocalIpAddress()
        {
            Assert.IsTrue(Network.IsLocalIpAddress("127.0.0.1"));
            Assert.IsTrue(Network.IsLocalIpAddress("localhost"));
            Assert.IsFalse(Network.IsLocalIpAddress("not.existant"));
        }
    }
}