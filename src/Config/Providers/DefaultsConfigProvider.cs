﻿using System;   
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Configuration
{
    /// <summary>
    /// Provides constant default values.
    /// </summary>
    public class DefaultConfigProvider
    {
        /// <summary>
        /// default playername
        /// </summary>
        public string PlayerName { get { return "Player"; } }
    }
}
