using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Utils;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using Launcher.Utils;
using System.Linq;
using System.ComponentModel;
using Serilog;
using Launcher.Models;
using Launcher.Controller;
using System;
using Avalonia.Input;
using System.Collections.ObjectModel;

namespace Launcher.GUI.Tabs
{
    public class SettingsTab : UserControl
    {
        protected DataGrid ClientListBox;
        protected MenuItem ClientPatchButton;

        public SettingsTab()
        {
            InitializeComponent();
            ClientListBox = this.Find<DataGrid>("SettingsTabClientGrid");
            ClientPatchButton = this.Find<MenuItem>("ClientPatchMenuItem");
            AddHandler(DragDrop.DropEvent, Drop);
            ClientListBox.SelectionChanged += (sender, args) => ClientPatchButton.IsEnabled = ClientListBox.SelectedItem != null && !((GameClient)ClientListBox.SelectedItem).IsPatched;
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void OnRegisterClientClick(object sender, RoutedEventArgs e)
        {
            RegisterClientWindow w = new RegisterClientWindow();
            w.Show();            
        }

        public void OnRemoveClientClick(object sender, RoutedEventArgs e)
        {            
            GameClient client = (GameClient)ClientListBox.SelectedItem;
            if (client == null)
                return;
            GameClient.Remove(c => c.InstallPath == client.InstallPath);
        }

        public void OnPatchClientClick(object sender, RoutedEventArgs e)
        {
            GameClient client = (GameClient)ClientListBox.SelectedItem;
            if(client != null)
            {
                PatchController.PatchGameFile(client,
                    () => {
                        MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Patching succesfull", "The patches have been applied!").Show();
                    },
                    () => MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Patching failed", "Patching the client has failed! Please see logs for more infos.").Show());
            }
        }

        public void Drop(object sender, DragEventArgs e)
        {
            try
            {
                if (!e.Data.Contains(DataFormats.FileNames) || e.Data.GetFileNames().Count() != 1 || !e.Data.GetFileNames().First().Contains(".exe"))
                    throw new UserInputException("Please drop only a single application file!");

                GameClient.Add(e.Data.GetFileNames().First());
            } catch (Exception ex) { ErrorHandler.Handle(ex); } 
        }
    }
}