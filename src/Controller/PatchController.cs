﻿using Launcher.Models;
using Launcher.Utils;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PeNet;

namespace Launcher.Controller
{
    class PatchController : IController
    {
        /// <summary>
        /// Patch a client
        /// </summary>
        /// <param name="client"></param>
        /// <param name="OnSuccess"></param>
        public static void PatchGameFile(GameClient client, Action OnSuccess = null, Action OnError = null)
        {
            var input = client.OriginalGameFilePath;

            bool succeed = false;
            PatchGameFile(input, Path.Join(client.BinaryPath, "BLR.exe") , true, true, () => succeed = true);

            if (succeed)
                OnSuccess();
            else
                OnError();
        }

        public async static Task<bool> DownloadProxy(GameClient client)
        {
            try
            {
                using(var wClient = new WebClient())
                {
                    wClient.DownloadFile("https://blrevive.superewald.net/releases/Proxy.dll", Path.Join(client.BinaryPath, "Proxy.dll"));
                }
            }
            catch(Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Create patched game file
        /// </summary>
        public static void PatchGameFile(string Input, string Output,
             bool patches = true, bool proxy = false, Action OnSuccess = null)
        {
            Log.Debug("Trying to patch {0} (ApplyPatches = {1}; InjectProxy = {2})", Path.GetFileName(Input), patches, proxy);

            FileStream patchedFile = null;
            BinaryWriter Bin = null;
            try
            {
                // we cant patch files that dont exist!
                if (!File.Exists(Input))
                {
                    Log.Error($"Cannot find game file ({Input})");
                    throw new UserInputException("Input file not found", new FileNotFoundException("", Input));
                }

                // patching file
                File.Copy(Input, Output, true);
                patchedFile = new FileStream(Output, FileMode.Open);
                Bin = new BinaryWriter(patchedFile);

                DisableASLR(Bin);
                if (patches)
                    Patch(Bin);

                Bin.Dispose();
                patchedFile.Dispose();

                if (proxy)
                    AddProxyIATEntry(Output);

                Log.Information("Succesfully patched and saved to {0}", Output);
                OnSuccess();
            }
            catch (Exception ex) when (
              ex.GetType() == typeof(FileNotFoundException) ||
              ex.GetType() == typeof(AccessViolationException)
          )
            {
                Log.Debug(ex, "Patching failed due to missing file (rights)");
                throw new UserInputException("Patching failed: either game file or directory is not readable/writable", ex);
            }
            catch (Exception ex) when (
              ex.GetType() == typeof(IOException) ||
              ex.GetType() == typeof(ArgumentException)
          )
            {
                Log.Debug(ex, "Patching failed due to input binary missmatch");
                throw new UserInputException($"Patching failed: The specified file does not provide valid offsets! {ex.Message}", ex);
            }
            catch (Exception ex) when (ex.GetType() != typeof(UserInputException))
            {
                Log.Fatal(ex, "Unhandled exception occured while patching!");
                throw;
            }
            finally
            {
                if (Bin != null)
                    Bin.Dispose();
                if (patchedFile != null)
                    patchedFile.Dispose();
            }
        }

        /// <summary>
        /// Saves a binary stream from embbed resource to a file.
        /// </summary>
        /// <param name="resource">resource namespace</param>
        /// <param name="output">output file</param>
        protected static void SaveToFileFromResourceStream(string resource, string output)
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resource))
            {
                if (stream == null)
                    throw new UserInputException("This file comes without bundled Proxy! The client will still be patched but without static proxy injection.");

                byte[] buff = new byte[stream.Length];
                stream.Read(buff, 0, (int)stream.Length);
                File.WriteAllBytes(output, buff);
            }
        }

        /// <summary>
        /// Add Proxy.dll to IAT of patched pe file
        /// </summary>
        /// <param name="file">patched pe file</param>
        protected static void AddProxyIATEntry(string file)
        {
            // add iat entry for proxy.dll
            var peFile = new PeFile(file);
            peFile.AddImport("Proxy.dll", "InitializeThread");

            // save modified binary
            File.WriteAllBytes(file, peFile.RawFile.ToArray());
        }

        /// <summary>
        /// Write the patches to the stream.
        /// </summary>
        /// <param name="fs">file stream to write in</param>
        protected static void Patch(BinaryWriter Bin)
        {
            // patch crash issue (setemblem patch)
            byte[] patch = { 0x90, 0x90, 0x90, 0x90 };
            Bin.Seek(0xB38BA6, SeekOrigin.Begin);
            Bin.Write(patch);

            Log.Debug("Succesfully applied patches");
        }

        protected static void DisableASLR(BinaryWriter Bin)
        {

            // disable aslr :)
            Bin.Seek(0x1FE, SeekOrigin.Begin);
            Bin.Write((byte)0x00);
            Log.Debug("Disabled ASLR");
        }
    }
}
