using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.Collections.Generic;
using Avalonia.Interactivity;
using Launcher.Utils;
using System.Linq;
using Launcher.Models;
using Launcher.Controller;
using System.Diagnostics;
using System;

namespace Launcher.GUI.Tabs
{ 
    public class ClientTab : UserControl
    {

        private TextBox UserNameInput;
        private ComboBox ClientSelect;

        private TextBox ServerAddressInput;
        private NumericUpDown ServerPortInput;
        private DataGrid CustomLaunchOptionsDataGrid;

        public ClientTab()
        {
            InitializeComponent();

            UserNameInput = this.Find<TextBox>("UserName");
            ClientSelect = this.Find<ComboBox>("ClientSelect");
            ServerAddressInput = this.Find<TextBox>("ServerAddress");
            ServerPortInput = this.Find<NumericUpDown>("ServerPort");
            CustomLaunchOptionsDataGrid = this.Find<DataGrid>("CustomOptionsGrid");
            App.Data.ClientLaunchOptionsGrid = CustomLaunchOptionsDataGrid;
            CustomLaunchOptionsDataGrid.CellEditEnded += OnEditClientLaunchOptions;
        }

        public void OnEditClientLaunchOptions(object sender, DataGridCellEditEndedEventArgs e)
        {
            if(e.EditAction == DataGridEditAction.Commit)
            {
                App.Data.UpdateClientLaunchOptions((ObservableDictionary<string, string>)((DataGrid)sender).Items);
            }
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void OnLaunchClick(object sender, RoutedEventArgs e)
        {
            try
            {
                GameClient client = (GameClient)ClientSelect.SelectedItem;
                if(client == null)
                    throw new UserInputException("A Client must be selected!");

                string Username = UserNameInput.Text;
                ushort port = Convert.ToUInt16(ServerPortInput.Value);

                GameClient.StartClient(client, new GameClientOptions(){
                    CustomAddress = $"{ServerAddressInput.Text}:{port}",
                    Server = new GameServerStatusInfo(new GameServer(ServerAddressInput.Text, port , "")),
                    UserName = Username,
                    Custom = Configuration.Config.Game.ClientLaunchOptions
                });
            } catch(UserInputException ex)
            {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", ex.Message).Show();
            }
        }
    }
}