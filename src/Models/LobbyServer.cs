﻿using Launcher.Utils;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Launcher.Models
{
    public class LobbyServer
    {
        public const string LobbyServerListURI = "https://gitlab.com/blrevive/super-server/-/snippets/2118153/raw/master/super-servers.txt";

        public Uri URI { get; private set; } 
        public override string ToString() => URI.ToString();

        public static async Task<List<LobbyServer>> GetAll(string lobbyServerListUrl = LobbyServerListURI)
        {
            try
            {
                using (var http = new HttpClient())
                {
                    var raw = await http.GetStringAsync(lobbyServerListUrl);
                    return raw.Split("\n").Select(h => new LobbyServer(h)).ToList();
                }
            } catch(Exception e)
            {
                Log.Debug(e, "Failed to retrieve lobby server list!");
                return null;
            }
        }

        public static async Task<List<GameServer>> GetAllServers()
        {
            try
            {
                var lobbyServers = await GetAll();
                var gameServers = new List<GameServer>();
                lobbyServers.ForEach(x => gameServers.AddRange(x.GetServers().Result));
                return gameServers;
            } catch(Exception e)
            {
                ErrorHandler.Handle(e);
                return null;
            }
        }

        public static async Task<List<GameServerStatusInfo>> GetAllServerInfos()
        {
            var gameServer = await GetAllServers();
            var infos = new List<GameServerStatusInfo>();
            gameServer.ForEach(x => infos.AddRange(x.PullStatusInfo().Result));
            return infos; 
        }

        public async Task<List<GameServer>> GetServers()
        {
            try
            {
                using(var http = new HttpClient())
                {
                    var raw = await http.GetStringAsync(URI);
                    var res = JsonSerializer.Deserialize<List<GameServer>>(raw);
                    return res;
                }
            } catch(Exception e)
            {
                Log.Debug(e, $"Failed to pull servers from {this}");
                return null;
            }
        }

        public LobbyServer(Uri uri) 
        { 
            URI = uri;
        }

        public LobbyServer(string url) : this(new Uri(url)) { }
    }
}
