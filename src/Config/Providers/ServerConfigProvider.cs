﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Configuration
{
    /// <summary>
    /// Configuration related to server.
    /// </summary>
    public class ServerConfigProvider : IConfigProvider
    {
        /// <summary>
        /// Delay between server and client startup.
        /// </summary>
        public int ServerStartupOffset { get; set; }

        /// <summary>
        /// The region of the server
        /// </summary>
        public List<string> Regions { get; set; }

        /// <summary>
        /// Default selected region
        /// </summary>
        public string DefaultRegion { get; set; }
    }
}
