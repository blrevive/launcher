using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Launcher.Configuration;
using Launcher.Utils;
using Avalonia.Interactivity;
using System;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Launcher.Models;
using Launcher.Controller;
using System.Threading.Tasks;
using Avalonia.Threading;
using System.Collections.ObjectModel;

namespace Launcher.GUI.Tabs
{
    public class LobbyTab : UserControl
    {
        private DataGrid LobbyGrid;
        private DataGrid FavoriteGrid;

        private TextBox ServerTitleInput;
        private TextBox ServerAddressInput;
        private NumericUpDown ServerPortInput;

        public LobbyTab()
        {
            InitializeComponent();
            
            LobbyGrid = this.Find<DataGrid>("LobbyGrid");
            FavoriteGrid = this.Find<DataGrid>("FavoriteServerGrid");
            ServerTitleInput = this.Find<TextBox>("ServerTitle");
            ServerAddressInput = this.Find<TextBox>("ServerAddress");
            ServerPortInput = this.Find<NumericUpDown>("ServerPort");

            FavoriteGrid.CellPointerPressed += (a, e) => FavoriteGrid.SelectedIndex = e.Row.GetIndex();
            LobbyGrid.CellPointerPressed += (a, e) => LobbyGrid.SelectedIndex = e.Row.GetIndex();
        }
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void ConnectToServer(object sender, RoutedEventArgs e, DataGrid grid)
        {
            try
            {
                var client = (GameClient)((MenuItem)sender).SelectedItem;
                var selectedServer = (GameServerStatusInfo)grid.SelectedItem;

                if(selectedServer == null)
                    throw new UserInputException("Please select a server!");

                var username = Config.User.Username;

                client.StartClient(new GameClientOptions() {
                    UserName = username,
                    Server = selectedServer
                });
            } catch(UserInputException ex) {
                ErrorHandler.Handle(ex);
            }
        }

        public void ConnectToOnlineServer(object sender, RoutedEventArgs e) => ConnectToServer(sender, e, LobbyGrid);
        public void ConnectToFavoriteServer(object sender, RoutedEventArgs e) => ConnectToServer(sender, e, FavoriteGrid);

        public void OnServerInstanceSaveClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var title = ServerTitleInput.Text;
                var address = ServerAddressInput.Text;
                var port = (ushort)ServerPortInput.Value;

                if (title == null || address == null || port == 0)
                    throw new UserInputException("Server Title, Address and Port are required!");

                GameServer.AddFavorite(title, address, port);
            }
            catch (UserInputException ex)
            {
                ErrorHandler.Handle(ex);
            }
        }

        public void OnServerInstanceRemoveClick(object sender, RoutedEventArgs e)
        {
            try 
            {
                var selectedInstance = (GameServerStatusInfo)FavoriteGrid.SelectedItem;
                if(selectedInstance == null)
                    throw new UserInputException("Please select a server!");

                GameServer.RemoveFavorite(selectedInstance);
            } catch(UserInputException ex) {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", ex.Message).Show();
            } catch(Exception ex) {
                Log.Fatal(ex, $"Unknown exception occured: {ex.Message}");
            }
        }

        public void OnLobbiesBackupClick(object sender, RoutedEventArgs e)
        {
            Config.HostsBackup.Hosts = Config.Hosts.Hosts;
            Config.Save(Config.Hosts);
        }

        public void OnLobbiesRestoreClick(object sender, RoutedEventArgs e)
        {
            Config.Hosts.Hosts = Config.HostsBackup.Hosts;
            Config.Save();
        }

        public async void OnLobbiesUpdateClick(object sender, RoutedEventArgs e)
        {
            App.Data.GameServers = new ObservableCollection<GameServerStatusInfo>();
            _ = Task.Run(async () => {
                try
                {
                    App.Data.GameServers = new ObservableCollection<GameServerStatusInfo>(await LobbyServer.GetAllServerInfos());
                }
                catch(UserInputException ex)
                {
                    await Dispatcher.UIThread.InvokeAsync(() => MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", ex.Message).Show());
                }
            });
        }
    }
}